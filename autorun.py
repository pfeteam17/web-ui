#!/usr/bin/python
import MySQLdb
from gluon import DAL, Field
from gpiozero import *
import time
import Adafruit_DHT

#Mapping of I-O
db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
cur = db.cursor()
cur.execute("SELECT name,pin FROM controls")
for row in cur.fetchall():
	if row[0] == 'lights':
		LIGHTS_PIN = int(row[1])
	elif row[0] == 'fans':
		FANS_PIN = int(row[1])
	elif row[0] == 'water_pump':
		WATER_PUMP_PIN = int(row[1])
	elif row[0] == 'heater':
		HEATER_PIN = int(row[1])
db.close()
		
io_lights = OutputDevice(LIGHTS_PIN,False,False)
io_fans = OutputDevice(FANS_PIN,False,False)
io_water_pump = OutputDevice(WATER_PUMP_PIN,False,False)
io_heater = OutputDevice(HEATER_PIN,False,False)

db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
cur = db.cursor()
cur.execute("SELECT name,pin,secondary_pin,m,b FROM sensors")
for row in cur.fetchall():
	if row[0] == 'dht':
		DHT_PIN = int(row[1])
		DHT_S_PIN = int(row[2])
	elif row[0] == 'ph':
		PH_PIN = int(row[1])
		PH_M = int(row[3])
		PH_B = int(row[4])
	elif row[0] == 'big_level':
		BIG_LEVEL_PIN = int(row[1])
		BIG_LEVEL_S_PIN = int(row[2])
		BIG_LEVEL_M = int(row[3])
		BIG_LEVEL_B = int(row[4])
	elif row[0] == 'small_level':
		SMALL_LEVEL_PIN = int(row[1])
		SMALL_LEVEL_S_PIN = int(row[2])
		SMALL_LEVEL_M = int(row[3])
		SMALL_LEVEL_B = int(row[4])
db.close()

io_ph = MCP3008(PH_PIN)
io_big_level = DistanceSensor(echo=BIG_LEVEL_PIN,trigger=BIG_LEVEL_S_PIN)
io_small_level = DistanceSensor(echo=SMALL_LEVEL_PIN,trigger=SMALL_LEVEL_S_PIN)

# Initialisation du programme
PROGRAM_FINISHED = False
no_recipe = True
first_clock = time.time()
last_clock = time.time()
last_pump = time.time()
last_log = time.time()
light_start = "07:00:00"
light_time = 10

while not PROGRAM_FINISHED:
	# Etats des actuateurs
	db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
	cur = db.cursor()
	cur.execute("SELECT name,state FROM controls")
	for row in cur.fetchall():
		if row[0] == 'lights':
			if row[1] == '1' or row[1] == 'T':
				io_lights.on()
			else:
				io_lights.off()
		elif row[0] == 'fans':
			if row[1] == '1' or row[1] == 'T':
				io_fans.on()
			else:
				io_fans.off()
		elif row[0] == 'water_pump':
			if row[1] == '1' or row[1] == 'T':
				io_water_pump.on()
			else:
				io_water_pump.off()
		elif row[0] == 'heater':
			if row[1] == '1' or row[1] == 'T':
				io_heater.on()
			else:
				io_heater.off()
	db.close()
	
	# Recette active
	db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
	cur = db.cursor()
	cur.execute("SELECT recipe_log.id, recipe.light_start, recipe.light_time,recipe.fans_start,recipe.fans_time,recipe.temperature_set_point FROM recipe, recipe_log WHERE recipe.id = recipe_log.recipe_id AND recipe_log.ended = 'F' ORDER BY recipe_log.id DESC LIMIT 1")
	if cur.rowcount == 0:
		no_recipe = True
	else:
		no_recipe = False
		for row in cur.fetchall():
			recipe_log_id = row[0]
			light_start = row[1]
			light_time = row[2]
			fans_start = row[3]
			fans_time = row[4]
			temperature_set_point = row[5]
	db.close()

	# Log des capteurs
	if ((time.time() - last_log) > 60):
		last_log = time.time()	
		# Temperature & humidite
		temp = None
		hum = None
		while ((temp is None or hum is None) or (temp < 10 or temp > 40) or (hum < 10 or hum > 100)) :
			hum, temp = Adafruit_DHT.read(DHT_S_PIN,DHT_PIN)	
		# pH
		ph = io_ph.value * PH_M + PH_B
		# Niveau gros bac
		big_level = io_big_level.distance * BIG_LEVEL_M + BIG_LEVEL_B
		# Niveau petit bac
		small_level = io_small_level.distance * SMALL_LEVEL_M + SMALL_LEVEL_B
		db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
		cur = db.cursor()
		if no_recipe:
			cur.execute("INSERT INTO temperature(created_at,value) VALUES (NOW(),%s)",(temp))
			cur.execute("INSERT INTO humidity(created_at,value) VALUES (NOW(),%s)",(hum))
			cur.execute("INSERT INTO ph(created_at,value) VALUES (NOW(),%s)",(ph))
			cur.execute("INSERT INTO big_level(created_at,value) VALUES (NOW(),%s)",(big_level))
			cur.execute("INSERT INTO small_level(created_at,value) VALUES (NOW(),%s)",(small_level))
		else:
			cur.execute("INSERT INTO temperature(recipe_log_id,created_at,value) VALUES (%s,NOW(),%s)",(recipe_log_id,temp))
			cur.execute("INSERT INTO humidity(recipe_log_id,created_at,value) VALUES (%s,NOW(),%s)",(recipe_log_id,hum))
			cur.execute("INSERT INTO ph(recipe_log_id,created_at,value) VALUES (%s,NOW(),%s)",(recipe_log_id,ph))
			cur.execute("INSERT INTO big_level(recipe_log_id,created_at,value) VALUES (%s,NOW(),%s)",(recipe_log_id,big_level))
			cur.execute("INSERT INTO small_level(recipe_log_id,created_at,value) VALUES (%s,NOW(),%s)",(recipe_log_id,small_level))
		db.commit()
		db.close()

	# Si en automatique
	db = MySQLdb.connect(host="localhost",user="logger",passwd="password",db="datalogger")
	cur = db.cursor()
	cur.execute("SELECT state FROM controls WHERE name='automatic'")
	state = cur.fetchone()
	if state[0] == '1' or state[0] == 'T':
		# Gestion de la pompe a eau
		if (time.time() - last_clock) > 60:
			if (time.time() - last_pump) < 10:
				cur.execute("UPDATE controls SET state='T' WHERE name='water_pump'")	
			else:
				cur.execute("UPDATE controls SET state='F' WHERE name='water_pump'")	
				last_clock = time.time()
			db.commit()
		else:
			last_pump = time.time()
		# Gestion de la lumiere
		if ~no_recipe:
			if (time.localtime().tm_hour > time.strptime(str(light_start),"%H:%M:%S").tm_hour) and (time.localtime().tm_hour < time.strptime(str(light_start),"%H:%M:%S").tm_hour+light_time):
				cur.execute("UPDATE controls SET state='T' WHERE name='lights'")
			else:
				cur.execute("UPDATE controls SET state='F' WHERE name='lights'")
			db.commit()
			# Gestion des ventilateurs
			if (time.localtime().tm_hour > time.strptime(str(fans_start),"%H:%M:%S").tm_hour) and (time.localtime().tm_hour < time.strptime(str(fans_start),"%H:%M:%S").tm_hour+fans_time):
				cur.execute("UPDATE controls SET state='T' WHERE name='fans'")
			else:
				cur.execute("UPDATE controls SET state='F' WHERE name='fans'")
			db.commit()
			# Gestion de la temperature
			cur.execute("SELECT value FROM temperature ORDER BY id DESC LIMIT 1")
			temp = cur.fetchone()
			if temp[0] < temperature_set_point:
				cur.execute("UPDATE controls SET state='T' WHERE name='heater'")
			else:
				cur.execute("UPDATE controls SET state='F' WHERE name='heater'")
			db.commit()
	db.close()
	time.sleep(0.5)
	if (time.time() - first_clock) > 36000:
		PROGRAM_FINISHED = True
	
io_lights.close()
io_fans.close()
io_water_pump.close()
io_heater.close()
io_small_level.close()
io_big_level.close()
io_ph.close()
