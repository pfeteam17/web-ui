# -*- coding: utf-8 -*-

from gluon.fileutils import read_file
from time import gmtime, strftime #strftime("%Y-%m-%d %H:%M:%S", gmtime())
import time
from gpiozero import *
import Adafruit_DHT

#Mapping of I-O
DHT_PIN = 4
LIGHTS_PIN = 17
FANS_PIN = 27
WATER_PUMP_PIN = 22
HEATER_PIN = 16
LEVEL_BIG_TRIGGER_PIN = 26
LEVEL_BIG_ECHO_PIN = 5
LEVEL_SMALL_TRIGGER_PIN = 13
LEVEL_SMALL_ECHO_PIN = 6



response.title = T('web2py Web Framework')
response.keywords = T('web2py, Python, Web Framework')
response.description = T('web2py Web Framework')

session.forget()
cache_expire = not request.is_local and 300 or 0


# @cache.action(time_expire=300, cache_model=cache.ram, quick='P')
def index():
    return response.render('default/monitoring.html')
	
def monitoring():
	last_hours = 24*60
	temp_data = db().select(db.temperature.created_at,db.temperature.value, orderby=~db.temperature.created_at, limitby=(0,last_hours)).as_json()
	hum_data = db().select(db.humidity.created_at,db.humidity.value, orderby=~db.humidity.created_at, limitby=(0,last_hours)).as_json()
	ph_data = db().select(db.ph.created_at,db.ph.value, orderby=~db.ph.created_at, limitby=(0,last_hours)).as_json()
	big_data = db().select(db.big_level.value, orderby=~db.big_level.created_at, limitby=(0,1))
	small_data = db().select(db.small_level.value, orderby=~db.small_level.created_at, limitby=(0,1))
	return dict(temp_data=temp_data,hum_data=hum_data,ph_data=ph_data,big_data=big_data,small_data=small_data)
	
def recipes():
	recipes = db().select(db.recipe.ALL)
	if db(db.recipe_log.ended == False).isempty():
		active_recipe_id = 0
	else:
		active_recipe = db(db.recipe_log.ended == False).select(db.recipe_log.recipe_id, orderby=~db.recipe_log.id, limitby=(0,1))
		active_recipe_id = active_recipe[0].recipe_id
	return dict(recipes=recipes,active_recipe_id=active_recipe_id)
	
def help():
    return response.render()
	
def active_recipe():
	#check if no active recipe
	#insert (activate recipe)
	return True
	
def control():
	if request.vars.name:
		control = db(db.controls.name == request.vars.name)
	else:
		return False
	if request.vars.state:
		if request.vars.state == "on":
			control.update(state=True)
		else:
			control.update(state=False)
		return request.vars.state
	return False
	
def controlstate():
	states = db().select(db.controls.name,db.controls.state)
	return XML(states)

def gettemp():
	last_hours = 24*60
	temp_data = db().select(db.temperature.created_at,db.temperature.value, orderby=~db.temperature.created_at, limitby=(0,last_hours)).as_json()
	return XML(temp_data)

def gethum():
	last_hours = 24*60
	hum_data = db().select(db.humidity.created_at,db.humidity.value, orderby=~db.humidity.created_at, limitby=(0,last_hours)).as_json()
	return XML(hum_data)

def getph():
	last_hours = 24*60
	ph_data = db().select(db.ph.created_at,db.ph.value, orderby=~db.ph.created_at, limitby=(0,last_hours)).as_json()
	return XML(ph_data)

def getbiglevel():
	big_data = db().select(db.big_level.value, orderby=~db.big_level.created_at, limitby=(0,1))
	return "{:10.2f}".format(big_data[0].value)

def getsmalllevel():
	small_data = db().select(db.small_level.value, orderby=~db.small_level.created_at, limitby=(0,1))
	return "{:10.2f}".format(small_data[0].value)
