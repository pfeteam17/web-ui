from time import gmtime, strftime #strftime("%Y-%m-%d %H:%M:%S", gmtime())

db = DAL('mysql://logger:password@localhost/datalogger')

db.define_table(
	'recipe',
	Field('name',length=50),
	Field('image_path',length=50),
	Field('grow_periode','integer'),
	Field('temperature_set_point','integer'),
	Field('humidity_set_point','integer'),
	Field('ph_set_point','double'),
	Field('big_container_set_point','integer'),
	Field('small_container_set_point','integer'),
	Field('light_start','time'),
	Field('light_time','integer'),
	Field('fans_start','time'),
	Field('fans_time','integer'),
	format = '%(name)s',
	redefine=True,
	migrate=True
)

db.define_table(
	'recipe_log',
	Field('recipe_id',db.recipe),
	Field('start_date','date'),
	Field('stop_date','date'),
	Field('ended','boolean',default=False),
	migrate=True
)

db.define_table(
	'temperature',
	Field('recipe_log_id',db.recipe_log),
	Field('created_at','datetime'),
	Field('value','double'),
	format = '%(value)s',
	redefine=True,
	migrate=True
)

db.define_table(
	'humidity',
	Field('recipe_log_id',db.recipe_log),
	Field('created_at','datetime'),
	Field('value','double'),
	format = '%(value)s',
	redefine=True,
	migrate=True
)

db.define_table(
	'ph',
	Field('recipe_log_id',db.recipe_log),
	Field('created_at','datetime'),
	Field('value','double'),
	format = '%(value)s',
	redefine=True,
	migrate=True
)

db.define_table(
	'big_level',
	Field('recipe_log_id',db.recipe_log),
	Field('created_at','datetime'),
	Field('value','double'),
	format = '%(value)s',
	migrate=True
)

db.define_table(
	'small_level',
	Field('recipe_log_id',db.recipe_log),
	Field('created_at','datetime'),
	Field('value','double'),
	format = '%(value)s',
	migrate=True
)

db.define_table(
	'log',
	Field('created_at','datetime'),
	Field('status'),
	Field('message'),
	format = '%(value)s',
	redefine=True,
	migrate=True
)

db.define_table(
	'controls',
	Field('name',length=50),
	Field('pin','integer'),
	Field('state','boolean'),
	format = '%(value)s',
	migrate = True
)

db.define_table(
	'sensors',
	Field('name',length=50),
	Field('pin','integer'),
	Field('secondary_pin','integer'),
	Field('m','double'),
	Field('b','double'),
	format = '%(value)s',
	migrate = True
)

if db(db.recipe).isempty():
	db.recipe.insert(name='Tomates',image_path='images/tomato.png',grow_periode=60,temperature_set_point=22,humidity_set_point=60,
					ph_set_point=6.2,big_container_set_point=40,small_container_set_point=15,
					light_start='07:00',light_time=10,fans_start='07:00',fans_time=10)
	db.recipe.insert(name='Piments',image_path='images/pepper.png',grow_periode=90,temperature_set_point=25,humidity_set_point=50,
					ph_set_point=6.2,big_container_set_point=40,small_container_set_point=15,
					light_start='07:00',light_time=10,fans_start='07:00',fans_time=10)

if db(db.log).isempty():
	db.log.insert(created_at=strftime("%Y-%m-%d %H:%M:%S", gmtime()), status='warning', message='Please activate a recipe to Growmatic your plants!')

if db(db.controls).isempty():
        db.controls.insert(name='lights',pin=17,state=False)
        db.controls.insert(name='fans',pin=27,state=False)
        db.controls.insert(name='water_pump',pin=22,state=False)
        db.controls.insert(name='heater',pin=16,state=False)
		
if db(db.sensors).isempty():
        db.sensors.insert(name='dht',pin=4,secondary_pin=11,state=False)
        db.sensors.insert(name='ph',pin=0,m=14,b=0)
        db.sensors.insert(name='big_level',pin=5,secondary_pin=26,m=100,b=0)
        db.sensors.insert(name='small_level',pin=6,secondary_pin=13,m=100,b=0)

