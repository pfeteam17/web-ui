on="ctrl fa fa-toggle-on"
off="ctrl fa fa-toggle-off"

function logValues() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     alert(this.responseText);
    }
  };
  xhttp.open("GET", "log_values", true);
  xhttp.send();
}

function control(dom){
  var automaticControl = document.getElementById("automatic");
  if (((automaticControl != null) && (automaticControl.className == off)) || dom.id == "automatic"){
    var xhttp = new XMLHttpRequest();
    if (dom.className == on) {
      state = "off";
    }else{
      state = "on";
    }
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        if (this.responseText){
          if (this.responseText == "on"){
	    dom.className = on;
	  }else{
	    dom.className = off;
	  }
	}
      }
    };
    xhttp.open("GET", "control?state="+state+"&name="+dom.id, true);
    xhttp.send();
  }
}

function control_toggle(dom){
  if (dom.className == on) {
	  dom.className = off;
  }else{
	  dom.className = on;
  }
}

function update_controls(){
    var controls = [];
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var c = this.responseText.split("\n");
        for (i=0; i<c.length ; i++){
          controls[i] = c[i].split(",");
          var html_ctrl = document.getElementById(controls[i][0]);
          if (html_ctrl != null){
            if (controls[i][1].indexOf("True") != -1){
              html_ctrl.className = on;
            }else{
              html_ctrl.className = off;
            }
          }
        }
      }
    }
    x.open("GET", "controlstate", true);
    x.send();
    t = setTimeout(function(){ update_controls() }, 1000);
}

function update_temperature(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        temperature = JSON.parse(this.response);
	drawTempChart();
      }
    }
    x.open("GET", "gettemp", true);
    x.send();  
    t = setTimeout(function(){ update_temperature() }, 30000);
}
function update_humidity(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        humidity = JSON.parse(this.response);
	drawHumChart();
      }
    }
    x.open("GET", "gethum", true);
    x.send();  
    t = setTimeout(function(){ update_humidity() }, 30000);
}
function update_ph(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        ph = JSON.parse(this.response);
	drawPhChart();
      }
    }
    x.open("GET", "gettemp", true);
    x.send();  
    t = setTimeout(function(){ update_ph() }, 30000);
}
function update_big_level(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var big_level = document.getElementById("big_level");
        if (big_level != null){
          big_level.innerHTML = this.responseText;
        }
      }
    }
    x.open("GET", "getbiglevel", true);
    x.send();  
    t = setTimeout(function(){ update_big_level() }, 30000);
}
function update_small_level(){
    var x = new XMLHttpRequest();
    x.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var small_level = document.getElementById("small_level");
        if (small_level != null){
          small_level.innerHTML = this.responseText;
        }
      }
    }
    x.open("GET", "getsmalllevel", true);
    x.send();  
    t = setTimeout(function(){ update_small_level() }, 30000);
}
