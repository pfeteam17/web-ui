# -*- coding: utf-8 -*-
{
'!langcode!': 'fr',
'!langname!': 'Français',
'"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN': '"update" est une expression optionnelle comme "champ1=\'nouvellevaleur\'". Vous ne pouvez mettre à jour ou supprimer les résultats d\'un JOIN',
'%d-%m-%Y': '%d-%m-%Y',
'%s %%{row} deleted': '%s lignes supprimées',
'%s %%{row} updated': '%s lignes mises à jour',
'%s selected': '%s sélectionné',
'%Y-%m-%d': '%Y-%m-%d',
'%Y-%m-%d %H:%M:%S': '%Y-%m-%d %H:%M:%S',
'(**%.0d MB**)': '(**%.0d MB**)',
'**%(items)s** %%{item(items)}, **%(bytes)s** %%{byte(bytes)}': '**%(items)s** %%{item(items)}, **%(bytes)s** %%{byte(bytes)}',
'**%(items)s** items, **%(bytes)s** %%{byte(bytes)}': '**%(items)s** items, **%(bytes)s** %%{byte(bytes)}',
'**not available** (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)': '**not available** (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)',
'?': '?',
'@markmin\x01An error occured, please [[reload %s]] the page': 'An error occured, please [[reload %s]] the page',
'``**not available**``:red (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)': '``**not available**``:red (requires the Python [[guppy http://pypi.python.org/pypi/guppy/ popup]] library)',
'About': 'À propos',
'Active': 'Active',
'Add': 'Ajouter',
'Automatic': 'Automatique',
'Big tank water level': 'Niveau eau grand bac',
'Close Menu': 'Fermer le menu',
'Controls': 'Contrôles',
'Dashboard': 'Tableau de bord',
'days': 'jours',
'Edit': 'Modifer',
'Enter a number between %(min)g and %(max)g': 'Enter a number between %(min)g and %(max)g',
'Enter an integer between %(min)g and %(max)g': 'Enter an integer between %(min)g and %(max)g',
'Fans': 'Ventilateurs',
'for': 'pour',
'Grow periode': 'Période de croissance',
'Growmatic': 'Plantautomate',
'Heating': 'Élément chauffant',
'Help': 'Aide',
'hours': 'heures',
'Humidity': 'Humidité',
'Lamp': 'Lumière',
'Lettuce': 'Letue',
'Level 1': 'Niveau 1',
'Level 2': 'Niveau 2',
'Light hours': 'Éclairage',
'Luminosity': 'Luminosité',
'Manual': 'Manuel',
'Menu': 'Menu',
'Monitoring': 'Surveillance',
'Name': 'Nom',
'New recipe': 'Nouvelle recette',
'OFF': 'ÉTEINT',
'ON': 'ALLUMÉ',
'Peppers': 'Piments',
'pH': 'pH',
'pH pump': 'Pompe de pH',
'Plantautomate': 'Plantautomate',
'Recipes of grow': 'Recettes de croissance',
'Small tank water level': 'Niveau eau petit bac',
'Start time': 'Heure de départ',
'Tank pump': 'Pompe du réservoir',
'Temperature': 'Température',
'Test': 'Test',
'Tomatoes': 'Tomates',
'Water': 'Eau',
'web2py Web Framework': 'web2py Web Framework',
'web2py, Python, Web Framework': 'web2py, Python, Web Framework',
}
