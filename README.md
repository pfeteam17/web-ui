# Procédure d’installation du projet Plantautomate #

Le projet de Plantautomate automatise le contrôle d’une serre de salon par Raspberry Pi. Il est conseillé d’utiliser le dernier modèle du microcontrôleur (Pi3-model B) avec une carte micro SD de plus de 16 GB. La section suivante détaille les étapes pour recréer le serveur complet.

## Installation de Raspbian Lite ##

Le site de Raspberry Pi (www.raspberrypi.org) contient toutes les informations nécessaire pour brûler le système d’exploitation sur la micro SD. La première configuration à faire est la configuration wi-fi si nécessaire. Encore une fois, suivre la procédure du site du fabricant (https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md). Petit rappel, le nom d’utilisateur par défaut est “pi” et le mot de passe est “raspberry”.

## Installation des outils nécessaires ##

Même si la distribution devrait être la plus récente, il est préférable de faire la mise-à-jour des pointeurs de code source avant de débuter :


```
#!

sudo apt-get update
sudo apt-get upgrade
```


Ensuite, il faut plusieurs outils dont l’environnement de programmation Python, ses composantes pour la broche d’entrées-sorties et MySQL ainsi que l’interface git pour la récupération du projet :


```
#!

sudo apt-get install build-essential python-dev python-gpiozero 
sudo apt-get install mysql-server python-mysqldb 
sudo apt-get git-core
```


Le projet utilise un capteur de Adafruit dont la librairie en Python peut être installée ainsi :


```
#!

git clone https://github.com/adafruit/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT
sudo python setup.py install
```


Avant de récupérer le projet, il faut créer une base de données et un utilisateur dont le projet utilise afin de bâtir sa structure de données. Voici comment préparer la base de données :

```
#!

mysql -u root -p -e “CREATE DATABASE datalogger; CREATE USER ’logger’@’localhost’ IDENTIFIED BY ’password’ ; GRANT ALL ON datalogger.* TO ’logger’@’localhost’;“
```



## Récupération du projet ##

Le projet se trouve dans un dépot libre sur le site de décentralisation de code Bitbucket.org. Avec un nom d’utilisateur, il est possible de tirer le code source sur le microcontrôleur :


```
#!

git clone https://[username]@bitbucket.org/pfeteam17/web-ui.git
```


Ensuite, afin que le projet démarre automatiquement au départ du Raspberry Pi, il faut créer une tâche dans le crontab :


```
#!

crontab -e
```


Et en ajoutant la ligne suivante à la fin :


```
#!

@ reboot sh ~/web-ui/startapp.sh
```


Pour plus d’information sur le fonctionnement de crontab, visitez : https://fr.wikipedia.org/wiki/Cron

En redémarrant votre microcontrôleur, vous pouvez visualiser l’interface et contrôler votre serre si elle est montée selon les devis en accédant à : http://raspberrypi:8000/plantautomate/default/monitoring
Si votre réseau n’a pas de DHCP, utilisez l’adresse IP du microcontrôleur au lieu du nom d’hôte.